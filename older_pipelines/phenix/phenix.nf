#!/usr/bin/env nextflow
/*

========================================================================================
                          PHEnix SNP Calling Pipeline
========================================================================================
 #### Homepage / Documentation
 https://github.com/phe-bioinformatics/PHEnix-nextflow
 #### Authors
 Anthony Underwood @bioinformat <anthony.underwood@phe.gov.uk>
 Aleksey Jironkin
 Ulf Schaefer
----------------------------------------------------------------------------------------
*/

// Pipeline version
version = '1.0'

reads = params.read_dir + '/' + params.fastq_pattern_match

/***************** Setup inputs and channels ************************/

// set up output directory
final_output_dir = file(params.output_dir)
// set up input  from reference sequnce
reference_sequence = file(params.reference)
// set up input for snp config file
snp_pipeline_config = file(params.snp_pipeline_config)
/*
 * Creates the `read_pairs` channel that emits for each read-pair a tuple containing
 * three elements: the pair ID, the first read-pair file and the second read-pair file
 */
Channel
  .fromFilePairs( reads )
  .ifEmpty { error "Cannot find any reads matching: ${params.reads}" }
  .set { read_pairs }

// split into to channels: one for pre-trim QC and one for trimming
read_pairs.into {reads_for_QC; reads_for_trimming}


log.info "======================================================================"
log.info "                  PHEnix SNP calling pipeline"
log.info "======================================================================"
log.info "Nextflow pipeline version   : ${version}"
log.info "Reads                       : ${reads}"
log.info "Reference                   : ${params.reference}"
log.info "SNP pipeline config         : ${params.snp_pipeline_config}"
log.info "======================================================================"
log.info "Outputs written to path     : ${params.output_dir}"
log.info "======================================================================"
log.info ""


/*
 * trim reads
 */

 process trimming {
     input:
     set pair_id, file(reads) from reads_for_trimming

     output:
     set pair_id, file("${pair_id}_forward_paired.fq.gz"), file("${pair_id}_reverse_paired.fq.gz") into trimmed_reads

     """
     java -jar /usr/local/trimmomatic/trimmomatic-0.36.jar PE -phred33 ${reads} ${pair_id}_forward_paired.fq.gz ${pair_id}_forward_unpaired.fq.gz ${pair_id}_reverse_paired.fq.gz ${pair_id}_reverse_unpaired.fq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
     """
 }

// prepare reference sequence
process prepare_reference {
  input:
  file ('reference.fasta') from reference_sequence

  output:
  file 'reference.*' into prepared_reference_files

  """
  phenix.py prepare_reference --mapper bwa --variant gatk --reference reference.fasta
  """
}

// run snp pipeline to create filtered vcfs
process create_filtered_vcf {
  publishDir "${final_output_dir}",
    mode: 'copy'

  input:
  file ('reference.fasta') from reference_sequence
  file reference_file from prepared_reference_files
  file('snp_pipeline_config.yml') from snp_pipeline_config
  set pair_id, file(for_reads), file(rev_reads) from trimmed_reads

  output:
  stdout create_filtered_vcf_stdout
  set pair_id, file("${pair_id}.filtered.vcf") into filtered_vcfs

  """
  echo "VCF creation for $pair_id"
  phenix.py run_snp_pipeline -r1 $for_reads -r2 $rev_reads -r reference.fasta --sample-name $pair_id -o . -c snp_pipeline_config.yml
  """
}

// combine vcfs into fasta
process create_fasta_from_vcfs {
  publishDir "${final_output_dir}",
    mode: 'copy'

  input:
  file(contig_files) from filtered_vcfs.collect { it[1] }

  output:
  file('snp.fasta')

  """
  phenix.py vcf2fasta -d . -o snp.fasta --sample-Ns-gaps-auto-factor 2
  """

}

