#!/usr/bin/env nextflow
/*

========================================================================================
                          GHRU SNP Calling Pipeline based on PHEnix
========================================================================================
 Based on the PHEnix software
 #### Homepage / Documentation
 https://github.com/phe-bioinformatics/PHEnix-nextflow
    #### Authors
    Anthony Underwood @bioinformat <anthony.underwood@phe.gov.uk>
    Aleksey Jironkin
    Ulf Schaefer
----------------------------------------------------------------------------------------
*/

// Pipeline version
version = '1.0'

def versionMessage(){
  log.info"""
  ==============================================
  PHEnix SNP Calling Pipeline version ${version}
  ==============================================
  """.stripIndent()
}
def helpMessage() {
    log.info"""
    Based on the PHE SNP calling software PHEnix https://phenix.readthedocs.io
    Mandatory arguments:
      --output_dir     Path to output dir "must be surrounded by quotes"
      --reference_sequence  The path to a fasta file the reference sequence to which the reads will be mapped
ptions as described https://phenix.readthedocs.io/en/latest/Calling%20SNPs.html#config and https://phenix.readthedocs.io/en/latest/Filters.html      --snp_pipeline_config  The path to a yml file containing the filtering o
    Optional arguments:
    Either input_dir and fastq_pattern must be specified if using local short reads or accession_number_file if specifying samples in the SRA for which the fastqs will be downloaded
      --input_dir      Path to input dir "must be surrounded by quotes". This must be used in conjunction with fastq_pattern
      --fastq_pattern  The regular expression that will match fastq files e.g '*_{1,2}.fastq.gz'
      --accession_number_file Path to a text file containing a list of accession numbers (1 per line)
      --depth_cutoff The estimated depth to downsample each sample to. If not specified no downsampling will occur
      --tree Whether to create a maximum likelihood tree
      --max_fraction_of_column_Ns The maximum fraction of Ns in any column within the SNP alignment (default 0.5)
      --max_fraction_of_column_gaps The maximum fraction of gaps in any column within the SNP alignment (default 0.5)
   """.stripIndent()
}

//  print help if required
params.help = false
// Show help message
if (params.help){
    versionMessage()
    helpMessage()
    exit 0
}

// Show version number
params.version = false
if (params.version){
    versionMessage()
    exit 0
}
/***************** Setup inputs and channels ************************/
params.input_dir = false
params.accession_number_file = false
params.fastq_pattern = false
params.output_dir = false
params.reference = false
params.snp_pipeline_config = false
params.depth_cutoff = false
params.tree = false
params.max_fraction_of_column_Ns = false 
params.max_fraction_of_column_gaps = false

// check if getting data either locally or from SRA
Helper.check_optional_parameters(params, ['input_dir', 'accession_number_file'])

//  check a pattern has been specified
if (params.input_dir){
  fastq_pattern = Helper.check_mandatory_parameter(params, 'fastq_pattern')
}

// set up output directory
output_dir = Helper.check_mandatory_parameter(params, 'output_dir') - ~/\/$/
// set up input from reference sequnce
reference_sequence = file(Helper.check_mandatory_parameter(params, 'reference'))
// set up input for snp config file
snp_pipeline_config = file(Helper.check_mandatory_parameter(params, 'snp_pipeline_config'))


// assign depth cutoff
if ( params.depth_cutoff ) {
  depth_cutoff = params.depth_cutoff
} else {
  depth_cutoff = false
}


log.info "======================================================================"
log.info "                  PHEnix SNP calling pipeline"
log.info "======================================================================"
log.info "Nextflow pipeline version   : ${version}"
if (params.accession_number_file){
  log.info "Accession File    : ${params.accession_number_file}"
} else if (params.input_dir){
  log.info "Fastq inputs      : ${params.input_dir}/${fastq_pattern}"
}
log.info "Reference                   : ${params.reference}"
log.info "SNP pipeline config         : ${params.snp_pipeline_config}"
log.info "======================================================================"
log.info "Outputs written to path     : ${params.output_dir}"
log.info "======================================================================"
log.info ""

// prepare reference sequence
process prepare_reference {
  tag {'prepare reference'}
  input:
  file ('reference.fasta') from reference_sequence

  output:
  file 'reference.*' into prepared_reference_files

  """
  phenix.py prepare_reference --mapper bwa --variant gatk --reference reference.fasta
  """
}


if (params.accession_number_file){
  // Fetch samples from ENA
  accession_number_file = params.accession_number_file - ~/\/$/
  Channel
      .fromPath(accession_number_file)
      .splitText()
      .map{ x -> x.trim()}
      .set { accession_numbers }

  process fetch_from_ena {
    tag { accession_number }
    
    publishDir "${output_dir}/fastqs",
      mode: 'copy',
      saveAs: { file -> file.split('\\/')[-1] }

    input:
    val accession_number from accession_numbers

    output:
    set accession_number, file("${accession_number}/*.fastq.gz") into raw_fastqs

    """
    enaDataGet -f fastq -as /aspera.ini ${accession_number}
    """
  }
} else if (params.input_dir) {
  input_dir = params.input_dir - ~/\/$/
  fastqs = input_dir + '/' + fastq_pattern
  Channel
    .fromFilePairs( fastqs )
    .ifEmpty { error "Cannot find any reads matching: ${fastqs}" }
    .set { raw_fastqs }
}

raw_fastqs.into {raw_fastqs_for_qc; raw_fastqs_for_trimming; raw_fastqs_for_length_assessment}

// Assess read length and make MIN LEN for trimmomatic 1/3 of this value
process determine_min_read_length {
  tag { pair_id }

  input:
  set pair_id, file(file_pair) from raw_fastqs_for_length_assessment

  output:
  set pair_id, stdout into min_read_length

  """
  seqtk sample -s123 ${file_pair[0]} 1000 | printf "%.0f\n" \$(awk 'NR%4==2{sum+=length(\$0)}END{print sum/(NR/4)/3}')
  """
}


/*
 * trim reads
 */

min_read_length_and_raw_fastqs = min_read_length.join(raw_fastqs_for_trimming)

 process trimming {
  tag { accession_number }

  cache 'lenient'
  
  publishDir "${output_dir}/trimmed_fastqs",
  mode: 'copy'

  input:
  set accession_number, min_read_length, file(reads) from min_read_length_and_raw_fastqs

  output:
  set accession_number, file("${accession_number}_1.trimmed.fastq.gz"), file("${accession_number}_2.trimmed.fastq.gz") into trimmed_fastqs_for_vcf_creation,trimmed_fastqs_for_genome_size_estimation, trimmed_fastqs_for_read_counting

  """
  java -jar /usr/local/trimmomatic/trimmomatic-0.36.jar PE -phred33 ${reads} ${accession_number}_1.trimmed.fastq.gz ${accession_number}_1_unpaired.trimmed.fastq.gz ${accession_number}_2.trimmed.fastq.gz ${accession_number}_2_unpaired.trimmed.fastq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:${min_read_length}
    """
 }

 // Genome Size Estimation
process genome_size_estimation {
  tag { pair_id }
  
  input:
  set pair_id, file(for_reads), file(rev_reads)  from trimmed_fastqs_for_genome_size_estimation

  output:
  set pair_id, file('mash_stats.out') into mash_output

  """
  mash sketch -o /tmp/sketch_${pair_id}  -k 32 -m 3 -r ${for_reads}  2> mash_stats.out
  """
}

def find_genome_size(pair_id, mash_output) {
  m = mash_output =~ /Estimated genome size: (.+)/
  genome_size = Float.parseFloat(m[0][1]).toInteger()
  return [pair_id, genome_size]
}

// channel to output genome size from mash output
mash_output.map { pair_id, file -> find_genome_size(pair_id, file.text) } set {genome_size_estimation_for_downsampling}

// Estimate total number of reads
process count_number_of_reads {
  tag { pair_id }
  
  input:
  set pair_id, file(for_reads), file(rev_reads) from trimmed_fastqs_for_read_counting

  output:
  set pair_id, file('seqtk_fqchk.out') into seqtk_fqchk_output

  """
  seqtk fqchk -q 25 ${for_reads} > seqtk_fqchk.out
  """
}

def find_total_number_of_reads(pair_id, seqtk_fqchk_ouput){
  m = seqtk_fqchk_ouput =~ /ALL\s+(\d+)\s/
  total_reads = m[0][1].toInteger() * 2 // the *2 is an estimate since number of reads >q25 in R2 may not be the same
  return [pair_id, total_reads]
}
read_counts = seqtk_fqchk_output.map { pair_id, file -> find_total_number_of_reads(pair_id, file.text) }

fastqs_and_genome_size_and_read_count = trimmed_fastqs_for_vcf_creation.join(genome_size_estimation_for_downsampling).join(read_counts).map{ tuple -> [tuple[0], tuple[1], tuple[2], tuple[3], tuple[4]]}

// run snp pipeline to create filtered vcfs
process create_filtered_vcf {
  tag { accession_number }
  publishDir "${output_dir}/filtered_vcfs",
    mode: 'copy'

  input:
  file ('reference.fasta') from reference_sequence
  file reference_file from prepared_reference_files
  file('snp_pipeline_config.yml') from snp_pipeline_config
  set accession_number, file(for_reads), file(rev_reads), genome_size, read_count from fastqs_and_genome_size_and_read_count

  output:
  stdout create_filtered_vcf_stdout
  set accession_number, file("${accession_number}.filtered.vcf") into filtered_vcfs

  script:
  if (depth_cutoff  && read_count/genome_size > depth_cutoff.toInteger()){
    downsampling_factor = depth_cutoff.toInteger()/(read_count/genome_size)
    """
    mkdir downsampled_fastqs
    seqtk sample  ${for_reads} ${downsampling_factor} | gzip > downsampled_fastqs/${for_reads}
    seqtk sample  ${rev_reads} ${downsampling_factor} | gzip > downsampled_fastqs/${rev_reads}
    echo "VCF creation for $accession_number"
    phenix.py run_snp_pipeline -r1 downsampled_fastqs/${for_reads} -r2 downsampled_fastqs/${rev_reads} -r reference.fasta --sample-name ${accession_number} -o . -c snp_pipeline_config.yml
    """
  } else {
    """
    echo "VCF creation for $accession_number"
    phenix.py run_snp_pipeline -r1 ${for_reads} -r2 ${rev_reads} -r reference.fasta --sample-name ${accession_number} -o . -c snp_pipeline_config.yml
    """
  }
}

// combine vcfs into fasta
if (params.max_fraction_of_column_Ns) {
  max_fraction_of_column_Ns = params.max_fraction_of_column_Ns
} else {
  max_fraction_of_column_Ns = 0.5
}

if (params.max_fraction_of_column_gaps) {
  max_fraction_of_column_gaps = params.max_fraction_of_column_gaps
} else {
  max_fraction_of_column_gaps = 0.5
}

process create_fasta_from_vcfs {
  cache 'lenient'

  tag { 'make fasta from vcfs'}

  publishDir "${output_dir}",
    mode: 'copy'

  input:
  file(contig_files) from filtered_vcfs.collect { it[1] }
  file ('reference.fasta') from reference_sequence

  output:
  file('snp_alignment.fasta') into snp_alignment

  script:
  """
  ref_len=`cat reference.fasta | awk '\$0 ~ ">" { c=0 } \$0 !~ ">" {c+=length(\$0);} END { print c; }'`
  phenix.py vcf2fasta -d . -o snp_alignment.fasta --sample-Ns auto --sample-gaps auto  --reflength \$ref_len --column-Ns ${max_fraction_of_column_Ns} --column-gaps ${max_fraction_of_column_gaps}
  """

}

if (params.tree) {
  process build_tree {
    tag {'build tree'}
    publishDir "${output_dir}",
      mode: 'copy'

    input:
    file('snp_alignment.fasta') from snp_alignment

    output:
    file('snp_alignment.fasta.treefile')
    file('snp_alignment.fasta.contree')
  
    """
    iqtree -s snp_alignment.fasta -alrt 1000 -bb 1000
    """
  }
}
workflow.onComplete {
  Helper.complete_message(params, workflow, version)
}

workflow.onError {
  Helper.error_message(workflow)
}

